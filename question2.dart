//Create an array to store all the winning apps of the MTN Business App of the Year Awards since 2012; a) Sort and print the apps by name;  b) Print the winning app of 2017 and the winning app of 2018.; c) the Print total number of apps from the array.

void main() {
  List winningApps = [
    "Khula",
    "FNB Banking App",
    "Snapscan",
    "Wumdrop",
    "Domestly",
    "Shyft",
    "Naked Insurance",
    "Easyequity",
    "Ambani",
    "Live Inspect"
  ]; //An array of MTN winning apps from 2012-2021
  winningApps.sort(); //sorting lists according to alphabetical order
  for (int i = 0; i < winningApps.length; i++) {
    print(
        winningApps[i]); //for loop for printing every app contained in the list
  }
  print(
      "There has been ${winningApps.length} winners of the MTN business academy since 2012");
}
