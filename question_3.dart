class WiningApps {
  String appname;
  String sector;
  String developer_name;
  int year;

  WiningApps(this.appname, this.sector, this.developer_name, this.year);

  String get appName => appname;

  void uppercase() {
    print(appname.toUpperCase());
  }
}

void main() {
  WiningApps Ambani = WiningApps("Ambani", "IT", "Mukundi Lambani", 2018);
  print("""
App name: ${Ambani.appName}
Sector: ${Ambani.sector}
Developer name: ${Ambani.developer_name}
Year: ${Ambani.year}
  """);
  Ambani.uppercase();
}
